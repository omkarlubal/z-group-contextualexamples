import unittest
import io
import sys
from contextual_similarity import get_similarity_score
from sorting_based_on_vocab import sort_based_on_vocab
from printing_priority_results import print_sorted_lists
from arrange_examples_based_on_contextual_similarity import sort_sentences_by_similarity


class TestSortSentencesBySimilarity(unittest.TestCase):

    def test_sort_sentences_by_similarity(self):
        sentences = ["I like table", "I like chair", "I like bananas"]
        input_sentence = "I like fruit"

        expected_output = ['I like bananas', 'I like table', 'I like chair']
        self.assertEqual(sort_sentences_by_similarity(sentences, input_sentence), expected_output)

        sentences = ["The quick brown fox", "The lazy dog", "The brown fox jumps over the lazy dog"]
        input_sentence = "The quick fox jumps over the lazy dog"

        expected_output = ["The brown fox jumps over the lazy dog", "The quick brown fox", "The lazy dog"]
        self.assertEqual(sort_sentences_by_similarity(sentences, input_sentence), expected_output)


if __name__ == '__main__':
    unittest.main()


class TestSimilarityScore(unittest.TestCase):

    def test_get_similarity_score(self):
        # Define test sentences and expected similarity score
        sentence1 = "The cat sat on the mat."
        sentence2 = "The dog sat on the rug."
        expected_similarity_score = 0.9356032

        # Compute similarity score using the get_similarity_score function
        similarity_score = get_similarity_score(sentence1, sentence2)

        # Verify that the computed similarity score is close to the expected similarity score
        print(similarity_score)
        print(expected_similarity_score)
        self.assertAlmostEqual(similarity_score, expected_similarity_score, places=4)


class TestSortBasedOnVocab(unittest.TestCase):

    def test_sort_based_on_vocab(self):
        # Test with provided examples
        vocab = ['Apple', 'Banana', 'orange', 'red', 'colour', 'fruits']
        ex = ['Apple and banana are fruits', 'orange and mango are fruits', 'Apple is red fruits but not banana']
        expected_output = ['Apple is red fruits but not banana', 'Apple and banana are fruits',
                           'orange and mango are fruits']
        self.assertEqual(sort_based_on_vocab(vocab, ex), expected_output)

        # Test with different vocabulary and example
        vocab1 = ['rose', 'jasmin', 'orchaid', 'red', 'purple', 'white']
        ex1 = ['roses are red flower', 'jasmin are white but not red',
               'jasmin and orchaid are white and purple respectively']
        expected_output1 = ['jasmin and orchaid are white and purple respectively', 'jasmin are white but not red',
                            'roses are red flower']
        self.assertEqual(sort_based_on_vocab(vocab, ex), expected_output)

        # Test with different vocabulary and example
        vocab2 = ['dhosa', 'idli', 'vada', 'upma', 'vada', 'white']
        ex2 = ['dhosa are white food item', 'vada are yellow', 'upma, idli and dhosa are white']
        expected_output2 = ['upma, idli and dhosa are white', 'dhosa are white food item', 'vada are yellow']
        self.assertEqual(sort_based_on_vocab(vocab, ex), expected_output)

        # Test with empty vocabulary and example
        vocab = []
        ex = []
        expected_output = []
        self.assertEqual(sort_based_on_vocab(vocab, ex), expected_output)
        self.assertEqual(sort_based_on_vocab(vocab1, ex1), expected_output1)
        self.assertEqual(sort_based_on_vocab(vocab2, ex2), expected_output2)


class TestPrintSortedLists(unittest.TestCase):
    def test_print_sorted_lists(self):
        sorted_by_similarity = ["apple", "banana", "cherry"]
        sorted_by_vocabulary = ["banana", "cherry", "date", "elderberry"]
        expected_output = ["apple", "banana", "cherry", "date", "elderberry"]

        # Redirect the output to a string buffer

        captured_output = io.StringIO()
        sys.stdout = captured_output

        # Call the function with the test inputs
        print_sorted_lists(sorted_by_similarity, sorted_by_vocabulary)
        output = captured_output.getvalue().strip().split('\n')

        # Assert that the function output matches the expected output
        self.assertListEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main(argv=['first-arg-is-ignored'], exit=False)
