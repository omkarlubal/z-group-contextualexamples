import sys
sys.path.insert(0,'..')

import unittest
import numpy as np
from contextual_similarity import get_similarity_score
from sorting_based_on_vocab import sort_based_on_vocab
from printing_priority_results import print_sorted_lists
from sort_examples_based_on_context import sort_sentences_by_similarity
class TestSortSentencesBySimilarity(unittest.TestCase):
    def test_sort_sentences_by_similarity(self):
        # Test case 1: Empty list of sentences
        sentences = []
        input_sentence = "I am a test sentence."
        try:
            sort_sentences_by_similarity(sentences, input_sentence)
        except ValueError as error:
            assert str(error) == "The list of sentences is empty."
        
        # Test case 2: Empty input sentence
        sentences = ["This is a test sentence.", "Another test sentence."]
        input_sentence = ""
        try:
            sort_sentences_by_similarity(sentences, input_sentence)
        except ValueError as error:
            assert str(error) == "The input sentence is empty."
        
        # Test case 3: Non-empty input and list of sentences
        sentences = ["This is a test sentence.", "Another test sentence.", "Yet another test sentence."]
        input_sentence = "This is a test sentence."
        expected_output = ["This is a test sentence.", "Another test sentence.", "Yet another test sentence."]
        assert sort_sentences_by_similarity(sentences, input_sentence) == expected_output
        
     
if __name__ == '__main__':
    unittest.main()

class TestSimilarityScore(unittest.TestCase):
    def test_get_similarity_score(self):
        # Test case 1: Same sentence
        sentence1 = "This is a test sentence."
        sentence2 = "This is a test sentence."
        expected_output = 1.0
        assert np.isclose(get_similarity_score(sentence1, sentence2), expected_output)

        # Test case 2: Completely different sentences
        sentence1 = "This is a test sentence."
        sentence2 = "I love eating pizza."
        expected_output = -0.032  # Example similarity score computed manually
        assert np.isclose(get_similarity_score(sentence1, sentence2), expected_output, rtol=1e-3)

        # Test case 3: Slightly different sentences
        sentence1 = "I love eating pizza."
        sentence2 = "I like to eat pizza."
        expected_output = 0.816  # Example similarity score computed manually
        assert np.isclose(get_similarity_score(sentence1, sentence2), expected_output, rtol=1e-3)


class TestPrintSortedLists(unittest.TestCase):
    def test_print_sorted_lists(self):
        sorted_by_similarity = ["apple", "banana", "cherry", "date"]
        sorted_by_vocabulary = ["banana", "cherry", "date", "elderberry", "fig"]
        expected_output = ["apple", "banana", "cherry", "date", "elderberry", "fig"]
        assert print_sorted_lists(sorted_by_similarity, sorted_by_vocabulary) == expected_output

class TestSortBasedOnVocab(unittest.TestCase):
    def test_sort_based_on_vocab(self):
        # Test case 1
        ex = ['Apple and banana are fruits', 'orange and mango are fruits', 'Apple is red fruitsbut not banana']
        vocab = ['Apple', 'Banana', 'orange', 'red', 'colour', 'fruits']
        assert sort_based_on_vocab(vocab, ex) == ['Apple and banana are fruits', 'Apple is red fruitsbut not banana', 'orange and mango are fruits']

        # Test case 2
        vocab = ['car', 'train', 'bus', 'transport', 'vehicle', 'road']
        ex = ['I am going to travel by car', 'I am going to travel by train', 'I am going to travel by bus', 'transport is important', 'vehicle is a mode of transportation', 'road is important for transportation']
        assert sort_based_on_vocab(vocab, ex) == ['I am going to travel by car', 'I am going to travel by train', 'I am going to travel by bus', 'vehicle is a mode of transportation', 'transport is important', 'road is important for transportation']

        # Test case 3
        vocab = ['book', 'read', 'write', 'study', 'knowledge']
        ex = ['I like to read books', 'writing is my hobby', 'I like to study for knowledge']
        assert sort_based_on_vocab(vocab, ex) == ['I like to read books', 'writing is my hobby', 'I like to study for knowledge']

        # Test case 4
        vocab = ['sun', 'moon', 'earth', 'planet', 'space', 'astronomy']
        ex = ['sun is a star', 'moon orbits around the earth', 'earth is a planet', 'astronomy is the study of space']
        assert sort_based_on_vocab(vocab, ex) == ['sun is a star', 'moon orbits around the earth', 'earth is a planet', 'astronomy is the study of space']
